import React from "react";
import "./App.css";
import TaiXiu_Game from "./Components/TaiXiu_Game";

function App() {
  return (
    <div className="App">
      <TaiXiu_Game />
    </div>
  );
}

export default App;
