import React, { Component } from "react";
import bg_game from "../assets/bgGame.png";
import KetQua from "./KetQua";
import XucSac from "./XucSac";

export default class TaiXiu_Game extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bg_game})`,
          height: "100vh",
          width: "100vw",
        }}
        className=" bg_game"
      >
        <div className="title text-center pt-5 display-4">Game Xuc Sac</div>
        <XucSac />
        <KetQua />
      </div>
    );
  }
}
