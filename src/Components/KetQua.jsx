import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME } from "./redux/constant/xucXacConstant";

class KetQua extends Component {
  render() {
    return (
      <div className="text-center pt-5 display-4">
        <button className="btn btn-success" onClick={this.props.handlePlayGame}>
          <span className="display-4">Play Game</span>
        </button>

        <div className="pt-5 text-white">
          <div className="display-4">
            Win times:{" "}
            <span className="text-success">{this.props.winTotal}</span>
          </div>
          <div className="display-4 pt-3">
            Total Playing times:{" "}
            <span className="text-danger">{this.props.totalPlayingTimes}</span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    winTotal: state.xucSacReducer.winTotal,
    totalPlayingTimes: state.xucSacReducer.totalPlayingTimes,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
