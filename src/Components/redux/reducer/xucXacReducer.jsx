import { CHOICE, PLAY_GAME, TAI, XIU } from "../constant/xucXacConstant";

let initialState = {
  mangXucSac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  choice: null,
  winTotal: 0,
  totalPlayingTimes: 0,
};

export const xucSacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHOICE: {
      state.choice = payload;
      return { ...state };
    }
    case PLAY_GAME: {
      let newXucSacArray = state.mangXucSac.map((item) => {
        const randomXucSac = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucSac/${randomXucSac}.png`,
          giaTri: randomXucSac,
        };
      });

      state.totalPlayingTimes++;

      let totalScore = newXucSacArray.reduce((total, item, index) => {
        return (total += item.giaTri);
      }, 0);

      if (
        (totalScore > 10 && state.choice === TAI) ||
        (totalScore <= 10 && state.choice === XIU)
      ) {
        state.winTotal++;
      }

      return { ...state, mangXucSac: newXucSacArray };
    }
    default:
      return state;
  }
};
