import React, { Component } from "react";
import { connect } from "react-redux";
import "../Components/game.css";
import { CHOICE, TAI, XIU } from "./redux/constant/xucXacConstant";

class XucSac extends Component {
  state = {
    active: null,
  };

  handleChangeActive = (value) => {
    this.setState({ active: value });
  };

  renderListXucSac = () => {
    return this.props.mangXucSac.map((item, index) => {
      return (
        <img
          src={item.img}
          alt=""
          key={index}
          style={{ width: 100, margin: 10 }}
        />
      );
    });
  };
  render() {
    let { active } = this.state;
    return (
      <div className="d-flex justify-content-between container pt-5">
        <button
          className="btn btn-danger"
          style={{
            width: 150,
            height: 150,
            fontSize: 40,
            transform: `scale(${active == TAI ? 1.5 : 1})`,
          }}
          onClick={() => {
            this.handleChangeActive(TAI);
            this.props.handleChoose(TAI);
          }}
        >
          Tai
        </button>
        <div>{this.renderListXucSac()}</div>
        <button
          className="btn btn-dark"
          style={{
            width: 150,
            height: 150,
            fontSize: 40,
            transform: `scale(${active == XIU ? 1.5 : 1})`,
          }}
          onClick={() => {
            this.handleChangeActive(XIU);
            this.props.handleChoose(XIU);
          }}
        >
          Xiu
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  mangXucSac: state.xucSacReducer.mangXucSac,
});

const mapDispatchToProps = (dispatch) => {
  return {
    handleChoose: (choice) => {
      dispatch({
        type: CHOICE,
        payload: choice,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucSac);
